jQuery(document).ready(function() {
	// Get the tabs, and when we have them... 
	$.get('/templates/tabs', function(data) {
		// ... replace the top of the page with them.
		jQuery('div.tabs.replace').replaceWith(data);
		
		/// Then, setup what they do when they're clicked on. 
		jQuery('.tabs .tab-links a').on('click', function(e)  {
			var currentAttrValue = jQuery(this).attr('href');
 			
 			// Show/Hide Tabs
			jQuery('.tabs ' + currentAttrValue).show().siblings().hide();
 
			// Change/remove current tab to active
			jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
 
			e.preventDefault();
			
			// Then, get the actual data for that tab. 
			// Which tab is it? 
			var tabWithoutHash = currentAttrValue.substring(1);
			
			// Replace them with data: 
 			$.get("/templates/" + tabWithoutHash + "-header", function(data) {
				$("tr." + tabWithoutHash + ".header").slice(1).remove();
				$("tr." + tabWithoutHash + ".header").replaceWith(data);
			});
			$.get("/data/"+tabWithoutHash+"/rows",function(data) {
				$("tr."+tabWithoutHash+".rows").slice(1).remove();
				$("tr."+tabWithoutHash+".rows").replaceWith(data);
			});
		});
		
		// Then, click on the active one! 
		jQuery('#first-tab').click();
	});
});
