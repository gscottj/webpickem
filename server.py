import bottle
from beaker.middleware import SessionMiddleware
from cork import Cork
import logging

logging.basicConfig(format='server.py - [%(asctime)s] %(message)s', level=logging.DEBUG)
log = logging.getLogger(__name__)
bottle.debug(True)


app = bottle.app()



# Serve up the styles:


@bottle.route('/styles/tabs')
def css_tabs():
	"""Serve the CSS for how the tabs on the main page should look."""
	return bottle.static_file("tabs.css", root='styles/')

@bottle.route('/styles/spreadsheet')
def css_spreadsheets():
	"""Serve the CSS for how a spreadsheet should look."""
	return bottle.static_file("spreadsheet.css", root="styles/")


# Serve up any javascript files:

@bottle.route('/js/tabs')
def js_tabs():
	"""Serve the JavaScript for making tabs dynamic."""
	return bottle.static_file("tabs.js", root="js/")



# Serve up standard parts of pages: 

@bottle.route('/templates/tabs')
def views_tabs():
	"""Serve the tabs across the top of the main page."""
	return bottle.static_file("tabs.tmpl", root="templates/")

@bottle.route('/templates/summary-header')
def views_summary_header():
	"""Serve the header for the summary page."""
	return bottle.static_file("summary-header.tmpl", root="templates/")

@bottle.route('/templates/week<weekNum>-header')
def views_week_header(weekNum):
	"""Serve the header for the week1 page."""
	return bottle.static_file(
		"week" + str(weekNum) + "-header.tmpl", 
		root="templates/"
		)



# Serve up data to fill the page with:  

@bottle.route('/data/summary/rows')
def data_summary_rows():
	"""Serve up the rows that should be filled in after the summary headers."""
	return bottle.static_file("summary_rows.html", root="data/")

@bottle.route('/data/week<weekNum>/rows')
def data_week_rows(weekNum):
	"""Serve up a week number's data."""
	return bottle.static_file(
		"week" + str(weekNum) + "_rows.html", 
		root="data/"
		)



# Serve up the main page: 

@bottle.route('/')
@bottle.view('index.html')
def main_page():
	"""Serve index.html from views"""
	return {}



def main():

    # Start the Bottle webapp
    bottle.run(app=app, quiet=False, reloader=True)

if __name__ == "__main__":
    main()
